
package ejercicios1;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args) {
        Random ran = new Random();
        Scanner sc = new Scanner(System.in);
        int aleatorio = ran.nextInt(10) + 1;
        
        int n = 3;
        while (n > 0){
            System.out.println("Adivine un numero del 1 al 10. Num de intentos restantes: " + n);
            int numero = sc.nextInt();
            if (numero == aleatorio){
                System.out.println("Ganó!");
                break;
            }
            else if (n == 1){
                System.out.println("Perdió.");
            }
            n--;
        }
        
    }

}
