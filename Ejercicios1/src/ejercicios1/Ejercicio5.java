
package ejercicios1;

import java.util.Scanner;

public class Ejercicio5 {
    
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        System.out.println("Ingrese la palabra numero 1: ");
        String a = sc.nextLine();
        
        System.out.println("Ingrese la palabra numero 2: ");
        String b = sc.nextLine();
        
        
        int lenA = a.length();
        int lenB = b.length();
        
        String afin3 = a.substring(lenA-3);
        String bfin3 = b.substring(lenB-3);
        
        String afin2 = a.substring(lenA-2);
        String bfin2 = b.substring(lenB-2);
        
        
        if (afin3.equals(bfin3)){
            System.out.println("Las palabras riman.");
        }
        
        else if (afin2.equals(bfin2)){
            System.out.println("Las palabras riman un poco.");
        }
        
        else {
            System.out.println("Las palabras no riman.");
        }
    }
}
