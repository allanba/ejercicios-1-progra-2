
package ejercicios1;

import java.util.Scanner;

public class Ejercicio2 {
     public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese el numero");
        int numero = sc.nextInt();
        
        int suma = 0;
        int n = 1 ;
        while(n < numero){
            
            if (numero % n == 0){
                suma = suma + n;
            }
            
            n++;
        }
        
        if ( suma == numero){
            System.out.println("El numero ingresado es perfecto");
        }
        else{
            System.out.println("El numero ingresado es imperfecto");
        }
    }

}
