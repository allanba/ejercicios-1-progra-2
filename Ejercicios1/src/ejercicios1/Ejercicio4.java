
package ejercicios1;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Ejercicio4 {
    static Scanner sc = new Scanner(System.in);
    static JOptionPane jop = new JOptionPane();
    static String [][] matriz = new String [0][2];
    
    public static void main(String[] args) {
        while(true){
            System.out.println("\033[30mAgregar personas ingrese 1. Para leer la matriz ingrese 2. Modificar una edad 3");
            String acc = sc.nextLine();
            
            if (acc.equals("1")){
                agregar(matriz);
            }
            else if (acc.equals("2")){
                leer(matriz);
            }
            else if (acc.equals("3")){
                modificarEdad(matriz);
            }
        }
    }
    
    public static void agregar(String[][] arreglo){
        System.out.println("Ingrese el nombre: ");
        String nombre = sc.nextLine();
        System.out.println("Ingrese la edad: ");
        String edad = sc.nextLine();
        
        String[][] nuevoArr = new String [(arreglo.length + 1)][2];
        int n = 0;
        while (n < arreglo.length){
            nuevoArr[n][0] = arreglo[n][0];
            nuevoArr[n][1] = arreglo[n][1];
            n++;
        }
        nuevoArr[(n)][0] = nombre;
        nuevoArr[(n)][1] = edad;
        matriz = nuevoArr;
    }
    
    public static void leer(String[][] arreglo){
        for (int a = 0; a < arreglo.length; a++){
            int edad = Integer.parseInt(arreglo[a][1]);
            
            if (edad % 2 == 0){
                String edadStr = Integer.toString(edad);
                System.out.println(arreglo[a][0] + " -" + "\033[34m " + edadStr + "\u001B[0m");
            }
            else if (edad % 2 != 0){
                String edadStr = Integer.toString(edad);
                System.out.println(arreglo[a][0] + " -" + "\033[31m "+edadStr + "\u001B[0m");
            }
        }
    }
    
    public static void modificarEdad(String[][] arreglo){
        if (matriz.length < 1){
            System.out.println("No hay elementos aún.");
        }
        
        else{
            System.out.println("Indique num de edad que desea modificar: ");

            for (int a = 0; a < arreglo.length; a++){
                System.out.println((a+1) +"- "+ arreglo[a][0]);
            }

            int nPersona = sc.nextInt();

            System.out.println("Ingrese la nueva edad: ");

            String nuedad = sc.nextLine();
            
            if (nuedad.equals("")){
                nuedad = jop.showInputDialog("Scanner falló/\nIngrese la nueva edad:");
            }

            matriz[nPersona-1][1] = nuedad;
        }
    }

}
